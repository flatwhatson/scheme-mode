# M-x scheme-mode

This repository maintains a backport of `scheme.el` from GNU Emacs
`master` branch for use with stable versions of Emacs.

## Installation

To install it manually, clone this repository and add the directory to
your Emacs `load-path`.  It will be preferred over the built-in version.

## Reconstruction

The commit history of `scheme.el` is extracted from the emacs repository
using `git-filter-repo`.  This unmodified history can be found in the
`upstream` branch of this repository.

``` shell
guix shell git git-filter-repo man-db # for git filter-repo --help

# Create a fresh clone of emacs
git clone git://git.savannah.gnu.org/emacs.git scheme-mode-upstream
# OR: git clone path/to/emacs --no-local scheme-mode-upstream

# Remove everything except scheme.el and COPYING
cd scheme-mode-upstream
git filter-repo \
 --path COPYING \
 --path lisp/progmodes/scheme.el \
 --path-rename 'lisp/progmodes/:'
```

## License

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program.  If not, see <https://www.gnu.org/licenses/>.

See [COPYING](COPYING) for details.
